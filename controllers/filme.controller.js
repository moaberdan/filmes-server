var models  = require('../models');
var fs = require('fs');
const Op = models.sequelize.Op;

exports.listaDeFilmes = (req, res, next) =>{
    const limit = parseInt(req.query.limit) || 100;
    const page = parseInt(req.query.page) || 1;
    const offset = limit * (page - 1);
    const like = req.query.like || '';
    
    models.Filme
    .findAll({
        attributes: ['id','image', 'nome', 'duracao'],
        include:[ { 
            model: models.Categoria, 
            as: 'categoria',
            where: { id: models.sequelize.col('Filme.categoriaId') }, 
        }],
        limit: limit,
        offset: offset,
        order: [['createdAt','DESC' ]],
        where:{
            nome:{
                [Op.like]: `%${like}%`
            }
        }
    })
    .then(filmes =>{
        filmes.forEach(filme =>{  
                    
            const bitmap = fs.readFileSync('./' + filme.image.path);            
            filme.image = "data:image/"+filme.image.type+";base64," + new Buffer(bitmap).toString('base64');
        });
        
        res.status(200).json(filmes);
    })
    .catch(error =>{
        res.status(500).json(error);        
    });
}

exports.novoFilme = (req, res, next) =>{
    
    const query = {};
    query.image = {
        path: req.file.path,
        type: req.file.mimetype.split('/')[1]
    };
    query.nome = req.body.nome;
    query.duracao = req.body.duracao;
    query.categoriaId = req.body.categoriaId;    
    
    models.Filme
        .build(query, {include:[ { model: models.Categoria, as: 'categoria' }]})
        .save()
        .then(data =>{
            res.status(200).json(data.dataValues);
        })
        .catch(error =>{
            res.status(500).json(error.parent.sqlMessage);       
            
        });  
}

exports.todasCategorias = (req, res, next) =>{     
    
    models.Categoria
    .findAll({
        attributes: ['id', 'nome'],
        order: [['nome','ASC']]
    })
    .then(categorias =>{
        res.status(200).json(categorias);
    })
    .catch(error =>{
        res.status(500).json(error);        
    }); 
}