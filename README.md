### Passos para executar o servidor

Entrar na pasta e executar os comandos:
```
npm install   // para instalar as dependências
npm start    // para executar o servidor
```
* O banco utilizado foi o MySql
* Editar o arquivo config.json dentro da pasta config para alterar o usuário e senha do banco, caso seja necessário 