'use strict';
module.exports = (sequelize, DataTypes) => {
  const Filme = sequelize.define('Filme', {
    image: DataTypes.JSON,
    nome: DataTypes.STRING,
    duracao: DataTypes.INTEGER,
    categoriaId: DataTypes.INTEGER
  }, {});

  Filme.associate = function(models) {
    Filme.belongsTo(models.Categoria,{
      as: 'categoria'
    })
  };

  Filme.sync().then(data =>{
    console.log('tabela Filme criada')
  }).catch(error => console.error(error));
  return Filme;
};