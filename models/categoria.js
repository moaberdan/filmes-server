'use strict';
module.exports = (sequelize, DataTypes) => {
  const Categoria = sequelize.define('Categoria', {
    nome: DataTypes.STRING
  }, {});
  Categoria.associate = function(models) {
    
    Categoria.hasMany(models.Filme, {
      foreignKey: 'categoriaId'
    });
  };

  Categoria.sync().then(data =>{
    console.log('tabela Categoria criada')
  }).catch(error => console.error(error));

  return Categoria;
};