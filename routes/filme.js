var express = require('express');
var router = express.Router();
var filmeController = require('../controllers/filme.controller');

const multer = require('multer'); 
const storage = multer.diskStorage({ 
    destination: (req, file, cb) => {    
        cb(null, 'uploads/');    
    }, 
    filename: (req, file, cb) => {    
        cb(null, Date.now()+'-'+file.originalname);    
    }
});
const upload = multer({ storage })

router.get('/lista', filmeController.listaDeFilmes);
router.get('/categoria/all', filmeController.todasCategorias);
router.post('/novo',upload.single('image') ,filmeController.novoFilme);

module.exports = router;
