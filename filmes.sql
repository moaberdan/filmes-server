-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: filmes
-- ------------------------------------------------------
-- Server version	5.7.21-1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Categoria`
--

DROP TABLE IF EXISTS `Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Categoria`
--

LOCK TABLES `Categoria` WRITE;
/*!40000 ALTER TABLE `Categoria` DISABLE KEYS */;
INSERT INTO `Categoria` VALUES (1,'Ação','2018-11-24 03:43:20','2018-11-24 03:43:20'),(2,'Ficção','2018-11-24 03:43:20','2018-11-24 03:43:20'),(3,'Terror','2018-11-24 03:43:20','2018-11-24 03:43:20'),(4,'Romance','2018-11-24 03:43:20','2018-11-24 03:43:20');
/*!40000 ALTER TABLE `Categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Filmes`
--

DROP TABLE IF EXISTS `Filmes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Filmes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` json DEFAULT NULL,
  `nome` varchar(255) DEFAULT NULL,
  `duracao` int(11) DEFAULT NULL,
  `categoriaId` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categoriaId` (`categoriaId`),
  CONSTRAINT `Filmes_ibfk_1` FOREIGN KEY (`categoriaId`) REFERENCES `Categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Filmes`
--

LOCK TABLES `Filmes` WRITE;
/*!40000 ALTER TABLE `Filmes` DISABLE KEYS */;
INSERT INTO `Filmes` VALUES (1,'{\"path\": \"uploads/1543031045571-200px-Silenthill_1996_poster.jpg\", \"type\": \"jpeg\"}','Silent Hill',100,3,'2018-11-24 03:44:05','2018-11-24 03:44:05'),(2,'{\"path\": \"uploads/1543031108799-a-ultimamusica.jpg\", \"type\": \"jpeg\"}','A Última Música',80,4,'2018-11-24 03:45:08','2018-11-24 03:45:08'),(3,'{\"path\": \"uploads/1543031134410-bela.jpg\", \"type\": \"jpeg\"}','A Bela e A Fera',110,4,'2018-11-24 03:45:34','2018-11-24 03:45:34'),(4,'{\"path\": \"uploads/1543031151851-blitz-capa-2.jpg\", \"type\": \"jpeg\"}','Blitz',120,1,'2018-11-24 03:45:51','2018-11-24 03:45:51'),(5,'{\"path\": \"uploads/1543031197774-cowboy.jpg\", \"type\": \"jpeg\"}','Cowboys e Alliens',90,2,'2018-11-24 03:46:37','2018-11-24 03:46:37'),(6,'{\"path\": \"uploads/1543031222141-gritos-do-alem.jpg\", \"type\": \"jpeg\"}','Gritos do Além',105,3,'2018-11-24 03:47:02','2018-11-24 03:47:02'),(7,'{\"path\": \"uploads/1543031240348-iron.jpg\", \"type\": \"jpeg\"}','Iron Man',130,1,'2018-11-24 03:47:20','2018-11-24 03:47:20'),(8,'{\"path\": \"uploads/1543031268959-prometheus-capa-dvd3.jpg\", \"type\": \"jpeg\"}','Prometheus',150,2,'2018-11-24 03:47:48','2018-11-24 03:47:48'),(9,'{\"path\": \"uploads/1543031286460-querido-john.jpg\", \"type\": \"jpeg\"}','Querido john',75,4,'2018-11-24 03:48:06','2018-11-24 03:48:06'),(10,'{\"path\": \"uploads/1543031311077-sangue-e-honra-capa-41.jpg\", \"type\": \"jpeg\"}','Sangue e Honra',200,1,'2018-11-24 03:48:31','2018-11-24 03:48:31'),(11,'{\"path\": \"uploads/1543031359741-triangulo.jpg\", \"type\": \"jpeg\"}','O Mistério do Triângulo das Bermudas',70,2,'2018-11-24 03:49:19','2018-11-24 03:49:19');
/*!40000 ALTER TABLE `Filmes` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-24  1:15:07
